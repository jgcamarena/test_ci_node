FROM node:14

WORKDIR /app

COPY package.json ./

RUN npm install

ADD . /app

EXPOSE 8000

CMD ["npm","start"]

