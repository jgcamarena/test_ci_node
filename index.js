'use strict'

const express=require('express');
const app=express();

const port=process.env.PORT ||8000;

app.get('/',(req,res)=>{
	res.status(200).send("Hello from web deployement App");
});

app.get('/hi',(req,res)=>{
        res.status(200).send("<h1>Hello in a cooler way from web deployement App</h1>");
});

app.get('/hi/bye',(req,res)=>{
        res.status(200).send("<h1 style='color:pink'>The most coolest way..Hello from web deployement App</h1>");
});

app.get('/hola',(req,res)=>{
	res.status(200).send("<h1 style='color:crimson'>Ara si. En valencià del bo. Funciona el Desplegament Continu</h1>");
});

app.listen(port,()=>{
	console.log("Listen on port ",port);
});
